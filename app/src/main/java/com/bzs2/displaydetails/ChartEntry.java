package com.bzs2.displaydetails;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Vector;

class ChartEntry {
    public String name;
    float value;

    static int writeToFile(Vector<ChartEntry> entries, String filename) {
        try {
            Writer fo = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(filename), StandardCharsets.UTF_8));
            for (ChartEntry entry : entries) {
                fo.write(entry.name + "\n");
                fo.write(entry.value + "\n");
            }
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
            return 1;
        }
        return 0;
    }

    static Vector<ChartEntry> loadFromFile(String filename) {
        File file = new File(filename);
        if(!file.exists()) return null;

        Vector<ChartEntry> entries = new Vector<>();

        try {
            BufferedReader fis = new BufferedReader(new InputStreamReader(
                    new FileInputStream(filename), StandardCharsets.UTF_8));

            String line;
            while ((line = fis.readLine()) != null) {
                ChartEntry entry = new ChartEntry();
                entry.name = line;
                line = fis.readLine();
                entry.value = Float.parseFloat(line);
                entries.add(entry);
            }
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entries;
    }
}
