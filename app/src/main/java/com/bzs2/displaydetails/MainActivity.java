package com.bzs2.displaydetails;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ConfigurationInfo;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;


public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private NetworkChangeListener networkChangeListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager viewPager = findViewById(R.id.container);
        viewPager.setAdapter(mSectionsPagerAdapter);
        viewPager.addOnPageChangeListener(this);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        networkChangeListener = new NetworkChangeListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkIfDataUpToDate();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeListener, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(networkChangeListener);
    }

    private void checkIfDataUpToDate() {
        if(PieChartFragment.isDataOutdated(getApplicationContext())) {
            new DashboardDownloaderTask(getApplicationContext(), new DashboardDownloaderTask.Listener() {
                @Override
                public void onDashboardsDownloaded() {
                    mSectionsPagerAdapter.notifyDataSetChanged();
                }
            }).execute();
        }
    }

    class NetworkChangeListener extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            checkIfDataUpToDate();
        }
    }

    public static class InNumbersFragment extends Fragment {

        public InNumbersFragment() {
        }

        public static InNumbersFragment newInstance() {
            return new InNumbersFragment();
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.in_numbers, container, false);

            Display display = getActivity().getWindowManager().getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();

            double width;
            double height;
            int density;
            float refreshRate;
            int rotation;

            display.getRealMetrics(metrics);
            width = metrics.widthPixels;
            height = metrics.heightPixels;

            ((TextView) rootView.findViewById(R.id.text_resolution_px))
                    .setText(String.format(Locale.ENGLISH,
                            "%d x %d", (int)width, (int)height));

            display.getMetrics(metrics);
            density = metrics.densityDpi;
            refreshRate = display.getRefreshRate();
            rotation = display.getRotation();
            ((TextView) rootView.findViewById(R.id.text_resolution_px_user))
                    .setText(String.format(Locale.ENGLISH,
                            "%d x %d", metrics.widthPixels, metrics.heightPixels));


            ((TextView) rootView.findViewById(R.id.text_density))
                    .setText(String.format(Locale.ENGLISH,
                            "%d dpi", density));

            String densityRating;
            if(density <= 140) densityRating = "Low dpi";
            else if (density <= 200) densityRating = "Medium dpi";
            else if (density <= 280) densityRating = "High dpi";
            else if (density <= 400) densityRating = "X-high dpi";
            else if (density <= 560) densityRating = "XX-high dpi";
            else densityRating = "XXX-high dpi";

            switch (display.getRotation()) {
                case Surface.ROTATION_0: rotation = 0; break;
                case Surface.ROTATION_90: rotation = 90; break;
                case Surface.ROTATION_180: rotation = 180; break;
                case Surface.ROTATION_270: rotation = 270;
            }

            ((TextView) rootView.findViewById(R.id.text_density_rating))
                    .setText(densityRating);

            ((TextView) rootView.findViewById(R.id.text_refresh_rate))
                    .setText(String.format(Locale.ENGLISH,
                            "%d Hz", (int)refreshRate));

            ((TextView) rootView.findViewById(R.id.text_rotation))
                    .setText(String.format(Locale.ENGLISH,
                            "%d\u00B0", rotation));

            width = metrics.widthPixels;
            height = metrics.heightPixels;
            double wi = width / (double)metrics.xdpi;
            double hi = height / (double)metrics.ydpi;
            double x = Math.pow(wi, 2);
            double y = Math.pow(hi, 2);
            double screenInches = Math.sqrt(x+y);

            ((TextView) rootView.findViewById(R.id.text_screen_size_rating))
                    .setText(String.format(Locale.ENGLISH,
                            "%.2f\"", screenInches));

            final ActivityManager activityManager =
                    (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
            final ConfigurationInfo configurationInfo =
                    activityManager.getDeviceConfigurationInfo();
            final int version = configurationInfo.reqGlEsVersion;

            int openGLMajor = version >> 16;
            int openGLMinor = version & 0xFFFF;
            String openGLSupport = String.format(Locale.getDefault(), "%d.%d", openGLMajor, openGLMinor);

            ((TextView) rootView.findViewById(R.id.text_opengl_version))
                    .setText(openGLSupport);

            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return InNumbersFragment.newInstance();
                case 1:
                    return PieChartFragment.newInstance(PieChartFragment.CHART_TYPE_ANDROID_OS);
                case 2:
                    return PieChartFragment.newInstance(PieChartFragment.CHART_TYPE_DENSITIES);
                case 3:
                    return PieChartFragment.newInstance(PieChartFragment.CHART_TYPE_OPENGL);
            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "DISPLAY IN NUMBERS";
                case 1:
                    return "ANDROID OS";
                case 2:
                    return "DENSITIES";
                case 3:
                    return "OPENGL";
            }
            return null;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            if(object instanceof PieChartFragment)
                ((PieChartFragment) object).updatePie();

            return super.getItemPosition(object);
        }
    }

    public void onPageScrollStateChanged(int state) {}

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    public void onPageSelected(int position) {
    }
}
