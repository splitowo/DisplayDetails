package com.bzs2.displaydetails;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class PieChartFragment extends Fragment {

    private final static String CHART_TYPE_KEY = "CHART_TYPE_KEY";

    public final static String[] DATA_FILE = {
            "data_densities",
            "data_opengl",
            "data_android_os"
    };

    private final static String[] COLORS_ANDROID = {
            "#a67700",
            "#a69900",
            "#999900",
            "#889900",
            "#779900",
            "#509900",
            "#389900",
            "#009944",
            "#009966",
            "#009977",
            "#009999",
            "#0099B0",
            "#0099C3",
            "#0082C3"
    };

    private final static String[] COLORS_DENSITIES = {
            "#00bbff",
            "#00aaff",
            "#0099ff",
            "#0088ff",
            "#0077ff",
            "#0066ff",
            "#0055ff",
            "#0044ff",
            "#0033ff"
    };

    private final static String[] COLORS_OPENGL = {
            "#f50057",
            "#f50075",
            "#f50093",
            "#f500B1",
            "#f500C9",
            "#f500E7"
    };

    public final static int CHART_TYPE_DENSITIES = 0;
    public final static int CHART_TYPE_OPENGL = 1;
    public final static int CHART_TYPE_ANDROID_OS = 2;

    public PieChartFragment() {
    }

    public static PieChartFragment newInstance(int chartType) {
        PieChartFragment pieChartFragment = new PieChartFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(CHART_TYPE_KEY, chartType);
        pieChartFragment.setArguments(bundle);
        return pieChartFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pie_chart_fragment, container, false);
        view.findViewById(R.id.buttonInformation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                switch(getArguments().getInt(CHART_TYPE_KEY)) {
                    case CHART_TYPE_DENSITIES:
                        builder.setMessage(R.string.informationDensity);
                        break;
                    case CHART_TYPE_OPENGL:
                        builder.setMessage(R.string.informationOpenGL);
                        break;
                    case CHART_TYPE_ANDROID_OS:
                        builder.setMessage(R.string.informationAndroid);
                        break;
                }
                builder.setPositiveButton("OK", null);
                builder.create().show();
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        updatePie();
    }

    public void updatePie() {
        if(getContext() == null) return;
        Vector<ChartEntry> entries = ChartEntry.loadFromFile(
                getContext().getFilesDir().getPath()
                        + "/" + DATA_FILE[getArguments().getInt(CHART_TYPE_KEY, 0)]);
        if(entries == null) return;

        View view = getView();
        if(view == null) return;
        PieChartView pieChart = view.findViewById(R.id.piechart);
        List<SliceValue> slices = new ArrayList<>();

        DecimalFormat decimalFormat = new DecimalFormat("###.##");

        for (int i = 0; i < entries.size(); i++) {
            SliceValue slice = new SliceValue();
            slice.setLabel(String.format(Locale.ENGLISH, "%s: %s%%",
                    entries.get(i).name, decimalFormat.format(entries.get(i).value)));
            slice.setValue(entries.get(i).value);

            switch (getArguments().getInt(CHART_TYPE_KEY, 0)) {
                case CHART_TYPE_DENSITIES:
                    slice.setColor(Color.parseColor(COLORS_DENSITIES[i]));
                    break;
                case CHART_TYPE_OPENGL:
                    slice.setColor(Color.parseColor(COLORS_OPENGL[i]));
                    break;
                case CHART_TYPE_ANDROID_OS:
                    slice.setColor(Color.parseColor(COLORS_ANDROID[i]));
            }

            slices.add(slice);
        }

        PieChartData pieChartData = new PieChartData();
        pieChartData.setValues(slices);
        pieChartData.setHasLabels(true);
        pieChart.setPieChartData(pieChartData);
        getView().findViewById(R.id.textView).setVisibility(View.GONE);
    }

    static boolean isDataOutdated(Context context) {
        long ONE_DAY = 86400000;
        File file = new File(context.getFilesDir().getPath()
                + "/" + DATA_FILE[0]);
        return !file.exists() || System.currentTimeMillis() - file.lastModified() > ONE_DAY;
    }
}