package com.bzs2.displaydetails;

import android.content.Context;
import android.os.AsyncTask;
import android.util.SparseArray;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class DashboardDownloaderTask extends AsyncTask<String, Integer, Integer> {

    private final String DATA_DIRECTORY;
    private static final String HOST_URL = "https://developer.android.com/";
    private static final String DASHBOARD_URL = HOST_URL + "about/dashboards/index.html";
    private static final SparseArray<String> androidVersions;
    private static final List<String> screenDensities;
    private final Listener listener;

    static {
        androidVersions = new SparseArray<>();
        androidVersions.put(8, "2.2 Froyo");
        androidVersions.put(9, "2.3 Gingerbread");
        androidVersions.put(10, "2.3.3 Gingerbread");
        androidVersions.put(11, "3.0 Honeycomb");
        androidVersions.put(12, "3.1 Honeycomb");
        androidVersions.put(13, "3.2 Honeycomb");
        androidVersions.put(14, "4.0.1 Ice Cream Sandwich");
        androidVersions.put(15, "4.0.3 Ice Cream Sandwich");
        androidVersions.put(16, "4.1.x Jelly Bean");
        androidVersions.put(17, "4.2.x Jelly Bean");
        androidVersions.put(18, "4.3.x Jelly Bean");
        androidVersions.put(19, "4.4 KitKat");
        androidVersions.put(21, "5.0 Lollipop");
        androidVersions.put(22, "5.1 Lollipop");
        androidVersions.put(23, "6.0 Marshmallow");
        androidVersions.put(24, "7.0 Nougat");
        androidVersions.put(25, "7.1 Nougat");
        androidVersions.put(26, "8.0 Oreo");
        androidVersions.put(27, "8.1 Oreo");
        androidVersions.put(28, "9.0 Pie");

        screenDensities = new ArrayList<>(Arrays.asList(
                "ldpi",
                "mdpi",
                "tvdpi",
                "hdpi",
                "xhdpi",
                "xxhdpi"
        ));
    }

    public interface Listener {
        void onDashboardsDownloaded();
    }

    DashboardDownloaderTask(Context context, Listener listener) {
        DATA_DIRECTORY = context.getFilesDir().getPath();
        this.listener = listener;
    }

    @Override
    protected Integer doInBackground(String... arg0) {
        if(android.os.Debug.isDebuggerConnected())
            android.os.Debug.waitForDebugger();
        return downloadDashboard();
    }

    @Override
    protected void onPostExecute(Integer n) {
        super.onPostExecute(n);
        listener.onDashboardsDownloaded();
    }

    private Integer downloadDashboard() {
        Vector<ChartEntry> osVector;
        Vector<ChartEntry> densitiesVector;
        Vector<ChartEntry> openGLVector;
        try {
            Document document = Jsoup.connect(DASHBOARD_URL).get();

            String iframeSrc = document.select("iframe").first().attr("src");
            Document iframe = Jsoup.connect(HOST_URL + iframeSrc).get();

            String script = iframe.select("body").select("script").get(1).data();

            osVector = extractOsData(script);
            densitiesVector = extractDensitiesData(script);
            openGLVector = extractOpenGLData(document);

            ChartEntry.writeToFile(osVector, DATA_DIRECTORY
                    + "/" + PieChartFragment.DATA_FILE[PieChartFragment.CHART_TYPE_ANDROID_OS]);
            ChartEntry.writeToFile(densitiesVector, DATA_DIRECTORY
                    + "/" + PieChartFragment.DATA_FILE[PieChartFragment.CHART_TYPE_DENSITIES]);
            ChartEntry.writeToFile(openGLVector, DATA_DIRECTORY
                    + "/" + PieChartFragment.DATA_FILE[PieChartFragment.CHART_TYPE_OPENGL]);
        } catch (IOException e) {
            e.printStackTrace();
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    private Vector<ChartEntry> extractOsData(String script) {
        Vector<ChartEntry> result = new Vector<>();

        Pattern pattern = Pattern.compile("\"api\":\\s+(\\d+),\\s+.+\",\\s+\"perc\":\\s*\"(\\d+\\.?\\d*)\"");
        Matcher matcher = pattern.matcher(script);
        ChartEntry jbEntry = new ChartEntry();
        jbEntry.name = "4.1 - 4.3 Jelly Bean";
        jbEntry.value = 0;
        while(matcher.find()) {
            ChartEntry entry = new ChartEntry();
            int apiNumber = Integer.parseInt(matcher.group(1));
            if(apiNumber >= 16 && apiNumber <= 18) {
                jbEntry.value += Float.parseFloat(matcher.group(2));
                if(apiNumber == 18)
                    result.add(jbEntry);
            }
            else {
                entry.name = androidVersions.get(Integer.parseInt(matcher.group(1)));
                entry.value = Float.parseFloat(matcher.group(2));
                result.add(entry);
            }
        }

        return result;
    }

    private Vector<ChartEntry> extractDensitiesData(String script) {
        Vector<ChartEntry> result = new Vector<>();

        for (String screenDensity : screenDensities) {
            ChartEntry entry = new ChartEntry();
            entry.name = screenDensity;
            entry.value = 0;
            Pattern pattern = Pattern.compile("\"" + screenDensity + "\":\\s*\"(\\d+\\.?\\d*)\"");
            Matcher matcher = pattern.matcher(script);
            while(matcher.find()) {
                entry.value += Float.parseFloat(matcher.group(1));
            }
            result.add(entry);
        }

        return result;
    }

    private Vector<ChartEntry> extractOpenGLData(Document document) {
        Vector<ChartEntry> result = new Vector<>();

        Element openGLElement = document.select("table").last();
        Elements trs = openGLElement.select("tr");
        trs.remove(0);

        for (Element tr : trs) {
            ChartEntry entry = new ChartEntry();
            Elements tds = tr.select("td");
            entry.name = tds.get(0).text();
            entry.value = Float.parseFloat(tds.get(1).text().replace("%", ""));
            result.add(entry);
        }

        return result;
    }
}
